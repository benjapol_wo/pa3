package unitconverter.application;

import unitconverter.units.Area;
import unitconverter.units.Length;
import unitconverter.units.Unit;
import unitconverter.units.Weight;

/**
 * An enum that manages all UnitType(s) this application knows.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.9
 */
public enum UnitType {
	LENGTH("Length") {
		@Override
		public Length[] getUnits() {
			return Length.getUnits();
		}

		@Override
		public Unit[] getAllUnits() {
			return Length.getAllUnits();
		}
	},
	AREA("Area") {
		@Override
		public Area[] getUnits() {
			return Area.getUnits();
		}

		@Override
		public Unit[] getAllUnits() {
			return Area.getAllUnits();
		}
	},
	WEIGHT("Weight") {
		@Override
		public Weight[] getUnits() {
			return Weight.getUnits();
		}

		@Override
		public Unit[] getAllUnits() {
			return Weight.getAllUnits();
		}
	};

	/** Name of the UnitType. */
	private String name;

	/**
	 * Construct a new UnitType
	 * 
	 * @param name
	 *            - name of the UnitType.
	 */
	private UnitType(String name) {
		this.name = name;
	}

	/**
	 * Get a String representation for the UnitType.
	 */
	public String toString() {
		return name;
	}

	/**
	 * Get known Unit(s) from a UnitType.
	 * 
	 * @return Known Unit(s) from a UnitType.
	 */
	public abstract Unit[] getUnits();

	/**
	 * Get all known Unit(s) from a UnitType.
	 * 
	 * @return All known Unit(s) from a UnitType.
	 */
	public abstract Unit[] getAllUnits();
}