package unitconverter.application;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import unitconverter.units.Unit;

/**
 * A simplified UnitConverterGUI. Eases conversions between different units.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.9
 */
public class UnitConverterGUI extends JFrame implements Runnable {
	/** A default serial version UID. */
	private static final long serialVersionUID = 1L;
	/** A UnitConverter instance used to convert between units. */
	private UnitConverter unitConverter;
	/** The current UnitType in use. */
	private UnitType currentUnitType;
	/** Combo boxes that contains unit, serve as unit pickers. */
	private JComboBox<Unit> topComboBox, bottomComboBox;
	/** The current Units that are in use, shown in the combo boxes. */
	private Unit[] currentUnits;
	/** The GUI will show additional units if this is true. */
	private boolean showUncommonUnits;
	/** True when the bottom half of input fields is modified. */
	private boolean bottomModified;
	/** AbstractAction(s) that will toggle uncommon units display on and off. */
	private Action showUncommonUnitsAction, hideUncommonUnitsAction;

	/**
	 * Construct the UnitConverterGUI. Initially set currentUnitType to the
	 * first UnitType enum entry.
	 * 
	 * @param unitConverter
	 *            - a UnitConverter needed to perform conversions
	 */
	public UnitConverterGUI(UnitConverter unitConverter) {
		super("Unit Converter");
		this.unitConverter = unitConverter;
		currentUnitType = UnitType.values()[0];
		currentUnits = currentUnitType.getUnits();
	}

	/**
	 * Run the UnitConverterGUI.
	 */
	@Override
	public void run() {
		initUIComponents();
		pack();
		setVisible(true);
	}

	/**
	 * Initialize all GUI components of UnitConverterGUI.
	 */
	private void initUIComponents() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		initMenuBar();
		setTitle(currentUnitType.toString() + " Converter");
		Container contentPane = getContentPane();
		JPanel ioPanel = new JPanel();
		JPanel[] subIoPanel = new JPanel[2];

		subIoPanel[0] = new JPanel();
		subIoPanel[1] = new JPanel();
		JPanel actionPanel = new JPanel();
		JTextField topTextField = new JTextField(20);
		JTextField bottomTextField = new JTextField(20);
		topComboBox = new JComboBox<Unit>(currentUnitType.getUnits());
		bottomComboBox = new JComboBox<Unit>(currentUnitType.getUnits());
		JButton convertButton = new JButton("Convert");
		JButton clearButton = new JButton("Clear");

		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		ioPanel.setLayout(new BoxLayout(ioPanel, BoxLayout.X_AXIS));
		subIoPanel[0].setLayout(new BoxLayout(subIoPanel[0], BoxLayout.Y_AXIS));
		subIoPanel[1].setLayout(new BoxLayout(subIoPanel[1], BoxLayout.Y_AXIS));
		actionPanel.setLayout(new BoxLayout(actionPanel, BoxLayout.X_AXIS));

		contentPane.add(ioPanel);
		ioPanel.add(subIoPanel[0]);
		subIoPanel[0].add(topTextField);
		subIoPanel[0].add(bottomTextField);
		ioPanel.add(subIoPanel[1]);
		subIoPanel[1].add(topComboBox);
		subIoPanel[1].add(bottomComboBox);
		contentPane.add(actionPanel);
		actionPanel.add(convertButton);
		actionPanel.add(clearButton);

		bottomModified = false;

		/**
		 * A KeyListener that will set bottomModified to true if the user typed
		 * in the bottomTextField.
		 */
		class BottomKeyListener implements KeyListener {
			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyTyped(KeyEvent e) {
				bottomModified = true;
			}
		}

		/**
		 * An Action that will set bottomModified to true if the user changes
		 * the value of the bottomComboBox.
		 */
		class RightComboListener extends AbstractAction {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				bottomModified = true;
			}
		}

		bottomTextField.addKeyListener(new BottomKeyListener());
		bottomComboBox.addActionListener(new RightComboListener());

		/**
		 * An Action that performs the conversion between units using the
		 * UnitConverter. If bottomModified is true, it will performs conversion
		 * from bottom-to-top, otherwise it will performs the conversion
		 * vice-versa. Will also set the input text color to red if
		 * NumberFormatException occurred.
		 */
		class ConvertAction extends AbstractAction {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (bottomModified && !bottomTextField.getText().isEmpty()) {
						topTextField
								.setText(String.format("%g", unitConverter
										.convert((Unit) bottomComboBox
												.getSelectedItem(),
												(Unit) topComboBox
														.getSelectedItem(),
												Double.valueOf(bottomTextField
														.getText()))));
					} else {
						bottomTextField.setText(String
								.format("%g",
										unitConverter.convert(
												(Unit) topComboBox
														.getSelectedItem(),
												(Unit) bottomComboBox
														.getSelectedItem(),
												Double.valueOf(topTextField
														.getText()))));
					}
					topTextField.setForeground(Color.BLACK);
					bottomTextField.setForeground(Color.BLACK);
				} catch (NumberFormatException e) {
					if (bottomModified && !bottomTextField.getText().isEmpty()) {
						bottomTextField.setForeground(Color.RED);
					} else {
						topTextField.setForeground(Color.RED);
					}
				}
				bottomModified = false;
			}
		}

		Action convertAction = new ConvertAction();
		topTextField.addActionListener(convertAction);
		bottomTextField.addActionListener(convertAction);
		convertButton.addActionListener(convertAction);

		/**
		 * An Action that clears all inputs and outputs. Resetting them to their
		 * default states.
		 */
		class ClearAction extends AbstractAction {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				topTextField.setText("");
				bottomTextField.setText("");
				topComboBox.setSelectedIndex(0);
				bottomComboBox.setSelectedIndex(0);
			}
		}
		clearButton.addActionListener(new ClearAction());
	}

	/**
	 * Initialize the menu bar of the UnitConverterGUI.
	 */
	private void initMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenu conversionMenu = new JMenu("Conversion");
		JMenuItem[] conversionUnitsMenuItems = new JMenuItem[UnitType.values().length];
		JMenuItem uncommonUnitsToggleMenuItem = new JMenuItem(
				"Show additional units");
		JMenuItem exitMenuItem = new JMenuItem("Exit");

		setJMenuBar(menuBar);
		menuBar.add(fileMenu);
		menuBar.add(conversionMenu);

		fileMenu.add(exitMenuItem);

		/**
		 * An Action that is triggered when the user change the conversion unit
		 * type.
		 */
		class ConversionUnitsMenuItemChangeAction extends AbstractAction {
			private static final long serialVersionUID = 1L;

			private int index;

			public ConversionUnitsMenuItemChangeAction(int index) {
				this.index = index;
			}

			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentUnitType = UnitType.values()[index];
				setTitle(currentUnitType.toString() + " Converter");
				updateComboBoxes();
			}
		}

		for (int i = 0; i < conversionUnitsMenuItems.length; i++) {
			conversionUnitsMenuItems[i] = new JMenuItem(
					UnitType.values()[i].toString());
			conversionUnitsMenuItems[i]
					.addActionListener(new ConversionUnitsMenuItemChangeAction(
							i));
			conversionMenu.add(conversionUnitsMenuItems[i]);
		}
		conversionMenu.addSeparator();

		/**
		 * An Action that will show uncommon units when triggered.
		 */
		class ShowUncommonUnitsToggleAction extends AbstractAction {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				showUncommonUnits = true;
				uncommonUnitsToggleMenuItem.setText("Hide additional units");
				uncommonUnitsToggleMenuItem
						.removeActionListener(showUncommonUnitsAction);
				uncommonUnitsToggleMenuItem
						.addActionListener(hideUncommonUnitsAction);
				updateComboBoxes();
			}
		}

		/**
		 * An Action that will hide uncommon units when triggered.
		 */
		class HideUncommonUnitsToggleAction extends AbstractAction {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				showUncommonUnits = false;
				uncommonUnitsToggleMenuItem.setText("Show additional units");
				uncommonUnitsToggleMenuItem
						.removeActionListener(hideUncommonUnitsAction);
				uncommonUnitsToggleMenuItem
						.addActionListener(showUncommonUnitsAction);
				updateComboBoxes();
			}
		}
		showUncommonUnitsAction = new ShowUncommonUnitsToggleAction();
		hideUncommonUnitsAction = new HideUncommonUnitsToggleAction();
		conversionMenu.add(uncommonUnitsToggleMenuItem);
		uncommonUnitsToggleMenuItem.addActionListener(showUncommonUnitsAction);

		/**
		 * An Action that quits the application by disposing the JFrame.
		 */
		class ExitAction extends AbstractAction {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		}
		exitMenuItem.addActionListener(new ExitAction());
	}

	/**
	 * Update the top and bottom combo boxes to show the latest currentUnits.
	 */
	private void updateComboBoxes() {
		if (showUncommonUnits) {
			currentUnits = currentUnitType.getAllUnits();
		} else {
			currentUnits = currentUnitType.getUnits();
		}
		topComboBox.removeAllItems();
		bottomComboBox.removeAllItems();
		for (Unit u : currentUnits) {
			topComboBox.addItem(u);
			bottomComboBox.addItem(u);
		}
	}
}