package unitconverter.application;

import unitconverter.units.Unit;

/**
 * A simple UnitConverter that calls Unit.convertTo() to performs Unit
 * conversion.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.9
 */
public class UnitConverter {
	/**
	 * Performs conversion from one Unit to another Unit, and times it with
	 * amount.
	 * 
	 * @param from
	 *            - the Unit to convert from
	 * @param to
	 *            - the Unit to convert to
	 * @param amount
	 *            - amount of the from Unit to convert
	 * @return A converted amount in to Unit.
	 */
	public double convert(Unit from, Unit to, double amount) {
		if (from.getClass() != to.getClass())
			throw new IllegalArgumentException(
					"Units don't have the same UnitType.");
		return from.convertTo(amount, to);
	}
}
