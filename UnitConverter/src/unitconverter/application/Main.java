package unitconverter.application;

import java.awt.EventQueue;

/**
 * Main class, entry point of the application.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.9
 */
public class Main {
	public static void main(String[] args) {
		final Runnable GUI = new UnitConverterGUI(new UnitConverter());
		EventQueue.invokeLater(GUI);
	}
}