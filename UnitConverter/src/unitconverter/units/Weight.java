package unitconverter.units;

/**
 * A Weight unit that contains the name of the unit and its conversion factor
 * comparing to one kilogram unit.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.9
 */
public class Weight extends Unit {
	/** Array of known Units. */
	private static final Weight[] UNITS = new Weight[] {
		// SI unit.
		new Weight("(SI) kilogram", 1.0),
		// Metric units.
		new Weight("(metric) kilotonne", 0.000001),
		new Weight("(metric) tonne", 0.001),
		new Weight("(metric) gram", 1000.0),
		new Weight("(metric) milligram", 1000000.0),
		new Weight("(metric) microgram", 1000000000.0),
		// Avoirdupois units.
		new Weight("(avdp) pound", 2.6792289),
		new Weight("(avdp) ounce", 32.150747),
		// Common Thai units.
		new Weight("(TH) hap", 0.016399895),
		new Weight("(TH) chang", 0.81999475),
		new Weight("(TH) tamlueng", 16.399895),
		new Weight("(TH) baht", 66.666667),
		new Weight("(TH) gold baht", 65.963061),
		new Weight("(TH) salueng", 262.39832)
	};

	/** Array of known uncommon Units. */
	private static final Weight[] UNCOMMON_UNITS = new Weight[] {
		// Uncommon metric units.
		new Weight("(metric) Earth kilonewton", 0.00980665205),
		new Weight("(metric) centner", 0.01),
		new Weight("(metric) Earth newton", 9.806652),
		new Weight("(metric) carat", 5000.0),
		new Weight("(metric) centigram", 100000.0),
		new Weight("(metric) atomic mass unit", 6.0221418E26),
		// Uncommon Avoirdupois units.
		new Weight("(avdp) dram", 257.20597),
		new Weight("(avdp) scruple", 771.61792),
		new Weight("(avdp) grain", 15432.358),
		// Troy units (for precious items). 
		new Weight("(troy) pound", 2.6792289),
		new Weight("(troy) ounce", 32.150747),
		new Weight("(troy) pennyweight", 643.01493),
		new Weight("(troy) carat", 4878.0488),
		new Weight("(troy) grain", 15432.358),
		new Weight("(troy) mite", 308647.17),
		new Weight("(troy) doite", 7407532.0),
		// Natural unit.
		new Weight("(natural) Planck mass", 45945087),
		// Japanese units.
		new Weight("(JP) kan", 0.26666667),
		new Weight("(JP) kin", 0.16666667),
		new Weight("(JP) hyakume", 2.6666667),
		new Weight("(JP) monnme", 266.66667),
		new Weight("(JP) fun", 2666.6667),
		// Chinese units.
		new Weight("(CN) dan", 0.02),
		new Weight("(CN) jin", 2.0),
		new Weight("(CN) liang", 20.0),
		new Weight("(CN) qian", 200.0),
		new Weight("(CN) fen", 2000.0),
		new Weight("(CN) li", 20000.0),
		new Weight("(CN) hao", 200000.0),
		new Weight("(CN) si", 2000000.0),
		new Weight("(CN) hu", 20000000.0),
		// Uncommon Thai units.
		new Weight("(TH) mayong", 131.19916),
		new Weight("(TH) fueang", 524.79664),
		new Weight("(TH) seek", 1049.5933),
		new Weight("(TH) siao", 2099.1866),
		new Weight("(TH) pai", 2099.1866),
		new Weight("(TH) ath", 4198.3731),
		new Weight("(TH) solos", 8396.7463),
		new Weight("(TH) bia", 419837.31),				
	};

	/**
	 * Get known Weight Units.
	 * 
	 * @return Known Weight Units.
	 */
	public static Weight[] getUnits() {
		return UNITS.clone();
	}

	/**
	 * Get uncommon known Weight Units.
	 * 
	 * @return Uncommon known Weight Units.
	 */
	public static Weight[] getUncommonUnits() {
		return UNCOMMON_UNITS.clone();
	}

	/**
	 * Get all Weight Units.
	 * 
	 * @return All Weight Units.
	 */
	public static Weight[] getAllUnits() {
		Weight[] returnArray = new Weight[UNITS.length + UNCOMMON_UNITS.length];
		System.arraycopy(UNITS, 0, returnArray, 0, UNITS.length);
		System.arraycopy(UNCOMMON_UNITS, 0, returnArray, UNITS.length,
				UNCOMMON_UNITS.length);
		return returnArray.clone();
	}

	/**
	 * Construct a new Weight Unit.
	 * 
	 * @param name
	 *            - name of the Unit
	 * @param factor
	 *            - value of the Unit when it's equals to one base Unit
	 */
	private Weight(String name, double factor) {
		super(name, factor);
	}
}