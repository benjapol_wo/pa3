package unitconverter.units;

/**
 * An Area unit that contains the name of the unit and its conversion factor
 * comparing to one square meter unit.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.9
 */
public class Area extends Unit {
	/** Array of known Units. */
	private static final Area[] UNITS = new Area[] {
		// Base unit (SI).
		new Area("(SI) sq. meter", 1.0),
		// Common metric units.
		new Area("(metric) sq. kilometer", 0.000001),
		new Area("(metric) sq. decimeter", 100.0),
		new Area("(metric) sq. centimeter", 10000.0),
		new Area("(metric) sq. millimeter", 1000000.0),
		// Common Imperial units (US/UK).
		new Area("(imperial) sq. mile", 0.00000038610216),
		new Area("(imperial) acre", 0.00024710538),
		new Area("(imperial) sq. yard", 1.19599),
		new Area("(imperial) sq. foot", 10.76391),
		new Area("(imperial) sq. inch", 1550.0031),
		// Thai units.
		new Area("(TH) rai", 0.000625),
		new Area("(TH) ngan", 0.0025),
		new Area("(TH) tarang wa", 0.25)
	};

	/** Array of known uncommon Units. */
	private static final Area[] UNCOMMON_UNITS = new Area[] {
		// Uncommon metric units.
		new Area("(metric) hectare", 0.001),
		new Area("(metric) decare", 0.001),
		new Area("(metric) are", 0.01),
		new Area("(metric) barn", 1E28),
		// Uncommon Imperial units. (US/UK)
		new Area("(imperial) township", 0.00000001072506),
		new Area("(imperial) homestead", 0.0000015444086),
		new Area("(imperial) rood", 0.00098842153),
		new Area("(imperial) sq. rod", 0.039536861),
		new Area("(imperial) perch", 0.039536861),
		// Japanese units.
		new Area("(JP) shaku", 30.25),
		new Area("(JP) go", 3.025),
		new Area("(JP) jo", 0.605),
		new Area("(JP) tsubo", 0.3025),
		new Area("(JP) bu", 0.3025),
		new Area("(JP) se", 0.010083333),
		new Area("(JP) tan", 0.0010083333),
		new Area("(JP) cho", 0.00010083333),
		// Chinese units.
		new Area("(CN) qing", 0.000015), new Area("(CN) mu", 0.0015),
		new Area("(CN) fen", 0.015), new Area("(CN) fang zhang", 0.09),
		new Area("(CN) li", 0.15), new Area("(CN) hao", 1.5),
		new Area("(CN) fang chi", 9.0), new Area("(CN) fang cun", 900.0)
	};

	/**
	 * Get known Area Units.
	 * 
	 * @return Known Area Units.
	 */
	public static Area[] getUnits() {
		return UNITS.clone();
	}

	/**
	 * Get uncommon known Area Units.
	 * 
	 * @return Uncommon known Area Units.
	 */
	public static Area[] getUncommonUnits() {
		return UNCOMMON_UNITS.clone();
	}

	/**
	 * Get all Area Units.
	 * 
	 * @return All Area Units.
	 */
	public static Area[] getAllUnits() {
		Area[] returnArray = new Area[UNITS.length + UNCOMMON_UNITS.length];
		System.arraycopy(UNITS, 0, returnArray, 0, UNITS.length);
		System.arraycopy(UNCOMMON_UNITS, 0, returnArray, UNITS.length,
				UNCOMMON_UNITS.length);
		return returnArray.clone();
	}

	/**
	 * Construct a new Area Unit.
	 * 
	 * @param name
	 *            - name of the Unit
	 * @param factor
	 *            - value of the Unit when it's equals to one base Unit
	 */
	private Area(String name, double factor) {
		super(name, factor);
	}
}