package unitconverter.units;

/**
 * An abstract superclass of all Units.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.9
 */
public abstract class Unit {
	/** Name of the Unit. */
	private String name;
	/** Value of one Unit compared to one base Unit. */
	private double factor;

	/**
	 * Construct a new Unit.
	 * 
	 * @param name
	 *            - name of the Unit
	 * @param factor
	 *            - value of the Unit when it's equals to one base Unit
	 */
	protected Unit(String name, double factor) {
		this.name = name;
		this.factor = factor;
	}

	/**
	 * Convert an amount from current Unit to another Unit.
	 * 
	 * @param amount
	 *            - an amount of the Unit that is converting from
	 * @param unit
	 *            - a Unit to convert to
	 * @return the amount converted to given Unit.
	 */
	public double convertTo(double amount, Unit unit) {
		return unit.getFactor() * amount / getFactor();
	}

	/**
	 * Get the factor of the Unit comparing to base Unit.
	 * 
	 * @return value of the Unit when it's equals to one base Unit.
	 */
	public double getFactor() {
		return factor;
	}

	/**
	 * Get the String representation of the Unit.
	 * 
	 * @return String representation of the Unit.
	 */
	public String toString() {
		return name;
	}
}
