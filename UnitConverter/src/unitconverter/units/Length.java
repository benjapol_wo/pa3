package unitconverter.units;

/**
 * A Length unit that contains the name of the unit and its conversion factor
 * comparing to one meter unit.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.9
 */
public class Length extends Unit {
	/** Array of known Units. */
	private static final Length[] UNITS = new Length[] {
		// Base unit (SI).
		new Length("(SI) meter", 1.00),
		// Metric units.
		new Length("(metric) kilometer", 0.001),
		new Length("(metric) decimeter", 10.0),
		new Length("(metric) centimeter", 100.0),
		new Length("(metric) millimeter", 1000.0),
		new Length("(metric) micrometer", 1000000.0),
		new Length("(metric) nanometer", 1000000000.0),
		// Common US Imperial units.
		new Length("(US) mile", 0.00062137119),
		new Length("(US) yard", 1.0936133),
		new Length("(US) foot", 3.2808399),
		new Length("(US) inch", 39.370079),
		// Common UK Imperial units.
		new Length("(UK) mile", 0.00062137227),
		new Length("(UK) yard", 1.0936152),
		new Length("(UK) foot", 3.2808456),
		new Length("(UK) inch", 39.370147),
		// Thai units.
		new Length("(TH) sen", 0.025),
		new Length("(TH) wa", 0.5), new Length("(TH) sok", 2.0),
		new Length("(TH) khuep", 4.0)
	};
	
	/** Array of known uncommon Units. */
	private static final Length[] UNCOMMON_UNITS = new Length[] {
		// Uncommon metric unit.
		new Length("(metric) angstrom", 10000000000.0),
		// Uncommon astronomical units (IAU defined).
		new Length("(IAU) light-year", 1.0 / 9460730472580800.0),
		new Length("(IAU) light-month", 12.0 / 9460730472580800.0),
		new Length("(IAU) light-day", 365.25 / 9460730472580800.0),
		// Uncommon US Imperial units.
		new Length("(US) league", 0.00020712373),
		new Length("(US) land", 0.00062137119),
		new Length("(US) bolt", 0.027340332),
		new Length("(US) pole", 0.19883878),
		new Length("(US) rod", 0.19883878),
		new Length("(US) perch", 0.19883878),
		new Length("(US) span", 4.3744608),
		new Length("(US) hand", 9.8425197),
		new Length("(US) line", 472.44094),
		new Length("(US) mil", 39370.079),
		new Length("(US) microinch", 39370.079),
		// US nautical units.
		new Length("(US nautical) mile", 0.00053959319),
		new Length("(US nautical) fathom", 0.54680556),
		// Uncommon UK Imperial units.
		new Length("(UK) league", 0.00020712409),
		new Length("(UK) land", 0.00062137227),
		new Length("(UK) skein", 0.00911346),
		new Length("(UK) bolt", 0.02734038),
		new Length("(UK) shackle", 0.03645384),
		new Length("(UK) furlong", 0.0049709782),
		new Length("(UK) chain", 0.049709782),
		new Length("(UK) rope", 0.16404228),
		new Length("(UK) pole", 0.19883913),
		new Length("(UK) goad", 0.7290768),
		new Length("(UK) ell", 0.87489216),
		new Length("(UK) pace", 1.3123382),
		new Length("(UK) cubit", 2.1872304),
		new Length("(UK) span", 4.3744608),
		new Length("(UK) nail", 4.3744608),
		new Length("(UK) shaftment", 6.5616912),
		new Length("(UK) hand", 9.8425368),
		new Length("(UK) palm", 13.123382),
		new Length("(UK) finger", 44.994454),
		new Length("(UK) digit", 52.49353),
		new Length("(UK) barleycorn", 118.11044),
		new Length("(UK) poppyseed", 472.44177),
		new Length("(UK) line", 472.44177),
		new Length("(UK) button", 472.44177),
		new Length("(UK) caliber", 3937.0147),
		new Length("(UK) thou", 39370.147),
		new Length("(UK) mil", 39370.147),
		// Uncommon UK nautical units.
		new Length("(UK admiralty) mile", 0.00053961182),
		new Length("(UK admiralty) cable length", 0.0053961182),
		// Uncommon Thai units.
		new Length("(TH) yot", 0.0000625),
		new Length("(TH) nio", 48.0),
		new Length("(TH) krabiat", 192.0)
	};

	/**
	 * Get known Length Units.
	 * 
	 * @return Known Length Units.
	 */
	public static Length[] getUnits() {
		return UNITS.clone();
	}

	/**
	 * Get uncommon known Length Units.
	 * 
	 * @return Uncommon known Length Units.
	 */
	public static Length[] getUncommonUnits() {
		return UNCOMMON_UNITS.clone();
	}

	/**
	 * Get all Length Units.
	 * 
	 * @return All Length Units.
	 */
	public static Length[] getAllUnits() {
		Length[] returnArray = new Length[UNITS.length + UNCOMMON_UNITS.length];
		System.arraycopy(UNITS, 0, returnArray, 0, UNITS.length);
		System.arraycopy(UNCOMMON_UNITS, 0, returnArray, UNITS.length,
				UNCOMMON_UNITS.length);
		return returnArray.clone();
	}

	/**
	 * Construct a new Length Unit.
	 * 
	 * @param name
	 *            - name of the Unit
	 * @param factor
	 *            - value of the Unit when it's equals to one base Unit
	 */
	private Length(String name, double factor) {
		super(name, factor);
	}
}